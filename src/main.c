/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <modal_pipe.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/development/development.h>

#include "context.h"
#include "qgc.h"
#include "config_file.h"

#define PROCESS_NAME		"voxl-mavcam-manager"

// This is the main data structure for the application. It is passed / shared
// with other modules as needed.
static context_data context;


static void print_help()
{
    printf("Usage: mavlink-camera-manager <options>\n");
    printf("Options:\n");
    printf("-c   load the config file only (used for scripted configuration)\n");
    printf("-h   print this help message\n");
    printf("-d   Enable debug messages\n");
    printf("-r   RTSP URI\n");
    printf("-n   camera name sent to GCS (default Camera0)\n");
    printf("\n");
    printf("camera name is NOT the MPA pipe name, that's set in the config file\n");
    printf("\n");
}


int main(int argc, char *argv[])
{
    int extra_options = 0;
    int command_line_option = 0;

    while ((command_line_option = getopt(argc, argv, "chdr:n:")) != -1) {
        switch (command_line_option) {
        case 'c':
            return config_file_read();
        case 'h':
            print_help();
            return 0;
        case 'd':
            context.debug = 1;
            break;
        case 'r':
            printf("Setting Manual RTSP server URL: %s\n", optarg);
            strncpy(context.rtsp_server_uri, optarg, RTSP_URI_MAX_SIZE);
            context.rtsp_manual_uri = 1;
            break;
        case 'n': // TODO this should be an integer number 0-6
            printf("Camera name: %s\n", optarg);
            strncpy(context.camera_name, optarg, CAMERA_NAME_MAX_SIZE);
            break;
        case ':':
            fprintf(stderr, "Error: Option needs a value\n");
            print_help();
            return 0;
        case '?':
            printf("unknown option: %c\n", optopt);
            print_help();
            return 0;
        }
    }

    for (; optind < argc; optind++) {
        fprintf(stderr, "Error: Extra argument: %s\n", argv[optind]);
        extra_options = 1;
    }

    if (extra_options) {
        print_help();
        return PROGRAM_ERROR;
    }

    if(config_file_read()){
        return PROGRAM_ERROR;
    }
    config_file_print();

    // make sure another instance isn't running
    // if return value is -3 then a background process is running with
    // higher privaledges and we couldn't kill it, in which case we should
    // not continue or there may be hardware conflicts. If it returned -4
    // then there was an invalid argument that needs to be fixed.
    if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

    // start signal manager so we can exit cleanly
    if(enable_signal_handler()==-1){
        fprintf(stderr,"ERROR: failed to start signal manager\n");
        return -1;
    }

    // make PID file to indicate your project is running
    // due to the check made on the call to rc_kill_existing_process() above
    // we can be fairly confident there is no PID file already and we can
    // make our own safely.
    make_pid_file(PROCESS_NAME);


    // Setup default camera name if one wasn't provided
    int camera_name_length = strlen(context.camera_name);
    if ( ! camera_name_length) {
        //                           01234567
        strcpy(context.camera_name, "Camera 0");
    }


    printf("Camera name for qgc: %s\n", context.camera_name);

    main_running = 1;
    context.compid = MAV_COMP_ID_CAMERA; // todo make this command line arg
    (void) qgc_init(&context);

    // Idle loop. The real work is done in the QGC module threads.   
    while (main_running) {
        usleep(5000000);
    }

    (void) qgc_shutdown();

    remove_pid_file(PROCESS_NAME);
    pipe_client_close_all();

    return 0;
}
