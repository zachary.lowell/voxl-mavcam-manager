/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <modal_json.h>
#include <modal_journal.h>
#include "config_file.h"

#define CONF_FILE "/etc/modalai/voxl-mavcam-manager.conf"

#define DEFAULT_SNAPSHOT_PIPE       "hires_snapshot"
#define DEFAULT_VIDEO_RECORD_PIPE   "hires_large_encoded"
#define DEFAULT_URI                 "rtsp://192.168.8.1:8900/live"

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration parameters for voxl-mavcam-manager.\n\
 * \n\
 * snapshot_pipe_name:       pipe to send snapshot comand to (default hires_snapshot)\n\
 * video_record_pipe_name:   pipe to pull h264/h265 data from for recording video\n\
 *                             to disk when the user clicks the record button in QGC\n\
 * default_uri:              this is the default uri to assume the rtsp stream is at\n\
 *                             Default value is rtsp://192.168.8.1:8900/live\n\
 * enable_auto_ip:           disable this to force the use of the default ip address\n\
 *                             when advertizing to the GCS. Otherwise we will use\n\
 *                             voxl-my-ip to auto-pick an ip address.\n\
 * \n\
 * To change the pipe that the live rtsp video stream is pulled from, edit\n\
 * /etc/modalai/voxl-streamer.conf instead.\n\
 *\n\
 */\n"


char snapshot_pipe_name[MODAL_PIPE_MAX_PATH_LEN];
char video_record_pipe_name[MODAL_PIPE_MAX_PATH_LEN];
char default_uri[MODAL_PIPE_MAX_PATH_LEN];
int  enable_auto_ip;


int config_file_print(void)
{
    printf("=================================================================");
    printf("\n");
    printf("Parameters as loaded from config file:\n");
    printf("snapshot_pipe_name:        %s\n", snapshot_pipe_name);
    printf("video_record_pipe_name:    %s\n", video_record_pipe_name);
    printf("default_uri:               %s\n", default_uri);
    printf("enable_auto_ip:            %d\n", enable_auto_ip);
    printf("=================================================================");
    printf("\n");
    return 0;
}



int config_file_read(void) {

    int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
    if(ret < 0) return -1;
    else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

    cJSON* parent = json_read_file(CONF_FILE);
    if(parent==NULL) return -1;

    json_fetch_string_with_default(parent, "snapshot_pipe_name", \
                                snapshot_pipe_name, \
                                MODAL_PIPE_MAX_PATH_LEN, \
                                DEFAULT_SNAPSHOT_PIPE);

    json_fetch_string_with_default(parent, "video_record_pipe_name", \
                                video_record_pipe_name, \
                                MODAL_PIPE_MAX_PATH_LEN, \
                                DEFAULT_VIDEO_RECORD_PIPE);

    json_fetch_string_with_default(parent, "default_uri", \
                                default_uri, \
                                MODAL_PIPE_MAX_PATH_LEN, \
                                DEFAULT_URI);

    json_fetch_bool_with_default(parent, "enable_auto_ip", &enable_auto_ip, 1);


    if(json_get_parse_error_flag()){
        fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
        cJSON_Delete(parent);
        return -1;
    }

    // write modified data to disk if neccessary
    if(json_get_modified_flag()){
        M_PRINT("writing %s to disk\n", CONF_FILE);
        json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
    }
    cJSON_Delete(parent);

    return 0;
}
